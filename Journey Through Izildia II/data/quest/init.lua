quest = {
  list={},
  data={}
}

quest.data = {}
quest.times = {}

local show = scene.list.slideshow
for name, dest in pairs(scene.list.map.roads) do quest.times[name] = 0 end

function quest.register(q)
  if q.enterLocation then Signal.register("enter-location", q.enterLocation) end
  if q.exitLocation then Signal.register("exit-location", q.exitLocation) end
  if q.nextScene then Signal.register("nextscene-location", q.nextScene) end
  if q.load then Signal.register("load-quest", q.load) end
  if q.battleFinish then Signal.register("battle-finish", q.battleFinish) end
end

function quest.pushFront(name)
  table.insert(show.queue, 1, name)
end

function quest.pushBack(name)
  table.insert(show.queue, name)
end

function quest.insert(n, name)
  table.insert(show.queue, show.index+n, name)
end

function quest.insertBefore(questname, name)
  --do something
end

function quest.insertAfter(questname, name)
  --do something
end

function quest.isEmpty()
  return next(show.queue) == nil
end

local songsrc = {}
function quest.playSong(path)
  songsrc[path] = songsrc[path] or love.audio.newSource("sound/music/" .. path)
  
  if music.path == path and music.source:isPlaying() then return end
  if music.isFading then return end
  if music.source:isPlaying() then music.source:stop() end
  
  music.path = path
  music.source = songsrc[path]
  music.volume = 1
  music.source:play()
  music.source:setLooping(true)
end

quest.list.main = require("quest.main")
quest.list.map =  require("quest.map")

return quest