local c_window_width = 960 -- We are using a hump camera, wich doesn't work on Sissors, that's why we use this. 

local Book = {}

local current = nil -- Stores the actual opened book (only once at time)
local animc = 6     -- Animation counter, from 0 to n page-flip, if c < n or c < 0 the animation starts. 

local img_page = love.graphics.newImage("gui/book/page.png")
local img_pageflip = {
  love.graphics.newImage("gui/book/page-flip1.png"),
  love.graphics.newImage("gui/book/page-flip2.png"),
  love.graphics.newImage("gui/book/page-flip3.png"),
  love.graphics.newImage("gui/book/page-flip4.png"),
  love.graphics.newImage("gui/book/page-flip5.png")
}
local img_undercover = love.graphics.newImage("gui/book/undercover.png")
local img_bookmark = love.graphics.newImage("gui/book/bookmark.png")

local font = love.graphics.newFont("gui/font/Daniel.otf", 30)

local sound_flip = love.audio.newSource("sound/flipping_pages.ogg", "static")

--[[
To use:

mybook = book.new("mybook.txt")
mybook:Open()
--]]

-- Declare table:shallowcopy() like: http://lua-users.org/wiki/CopyTable

local function parse(self, text)
  _, text = font:getWrap(text, 650)
  local p = 0
  for i=1, #text do
    if i % 20 == 1 then p = p + 1 end -- nº lines each page.
    if self.page[p] == nil then self.page[p] = "" end
    self.page[p] = self.page[p] .. text[i] .. "\n"
  end
  return text
end

function Book.new(filepath)
  local c = table.shallowcopy(Book)
  local text = ""
  if filepath then
    if language then filepath = "lang/" .. language .. '/' .. filepath end
    local file = io.open(filepath)
    for line in file:lines() do
      text = text .. line .. "\n"
    end
  end
  c:SetText(text)
  return c
end

function Book.SetText(self, text)
  self.pagenum = 1
  self.page = {}
  self.text = parse(self, text)
end

function Book:GetText()
  return self.text
end

function Book:Open()
  if loveframes then loveframes.SetState("Book") end
  if current ~= nil then current:Close() end
  current = self
end

function Book:NextPage()
  if self ~= current then return end
  local sp = #self.page
  if sp % 2 == 0 then sp = sp - 1 end
  if self.pagenum < sp then
    animc = 1
    self.pagenum = self.pagenum + 2
    sound_flip:stop()
    sound_flip:play()
  end
end

function Book:PrevPage()
  if self ~= current then return end
  if self.pagenum > 1 then
    animc = -#img_pageflip
    self.pagenum = self.pagenum - 2
    sound_flip:stop()
    sound_flip:play()
  end
end

function Book:Close()
  if loveframes then loveframes.SetState("none") end
  current = nil
  if self.closefunc then self:closefunc() end  
end

--------------------------

function Book.draw()
  if not current then return end
  local prop = {100, 0, 0, 0.75, 0.75}
  
  love.graphics.draw(img_undercover, unpack(prop))
  love.graphics.draw(img_page, unpack(prop))
  
  local p = current.pagenum
  local left_page =  {130, 60, 0, 0.5, 0.5}
  local right_page = {490, 60, 0, 0.5, 0.5}
  local jk = window_width / c_window_width
  
  function print_page(p)
    love.graphics.setColor(50,50,50,255)
    if current.page[p]   then love.graphics.print(current.page[p]  , unpack(left_page )) end
    if current.page[p+1] then love.graphics.print(current.page[p+1], unpack(right_page)) end
  end
  
  function draw_page(i, adv)
    i = math.floor(i)
    if i < 1 or i > #img_pageflip then return end
    love.graphics.draw(img_pageflip[i], unpack(prop))
    
    local x, y, w, h = 0, 0, window_width-140, window_height
    local tframe = {10, 80, 260, 540, 670}
    
    if adv then
      love.graphics.setScissor(x, y, w-tframe[i]*jk, h)
      print_page(p-2)
      love.graphics.setScissor(w-tframe[i]*jk, y, window_width, h)
    else
      love.graphics.setScissor(w-tframe[i]*jk, y, window_width, h)
      print_page(p+2)
      love.graphics.setScissor(x, y, w-tframe[i]*jk, h)
    end
  end
  
  love.graphics.setFont(font)
  
  if animc >= 1 and animc < #img_pageflip+1 then
    draw_page(animc, true)
    animc = animc + 0.15
  elseif animc < 0 and animc >= -#img_pageflip then
    draw_page(-animc, false)
    animc = animc + 0.15
  else
    animc = 6
    love.graphics.setScissor(100*jk, 0, window_width - 240*jk, window_height)
  end
  
  -- Print current pages
  print_page(p)
  
  -- Cleanup
  love.graphics.setColor(255,255,255,255)
  love.graphics.setScissor(0, 0, window_width, window_height)
end

function Book.mousereleased(x, y, button)
  if current == nil then return end
  local c = window_width/2
  if button == 1 then  
    if x > c then current:NextPage() end
    if x < c then current:PrevPage() end
  end
  if button == 2 then
    current:Close()
  end
end

Book = Book.new()
return Book