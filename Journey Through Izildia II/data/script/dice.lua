math.randomseed(os.time())

local dice = {
  all = "vwxyz",
  last = {}
}

function dice:getString()
  local text = ""
  if type(self.v) == "number" then text = text .. "V:" .. self.v .. " " end
  if type(self.w) == "number" then text = text .. "W:" .. self.w .. " " end
  if type(self.x) == "number" then text = text .. "X:" .. self.x .. " " end
  if type(self.y) == "number" then text = text .. "Y:" .. self.y .. " " end
  if type(self.z) == "number" then text = text .. "Z:" .. self.z .. " " end
  return text
end

function dice:roll(op)
  local all = self.all
  if op == nil then op = all end
  
  for c in all:gmatch"." do
    self.last[c] = self[c]
  end
  
  for c in op:gmatch"." do
    self[c] = math.random(0,9)
  end
  
  return self:getString()
end

function dice:nilify(op)
  local all = self.all
  if op == nil then op = all end
  
  for c in op:gmatch"." do
    self[c] = nil
  end
end

return dice